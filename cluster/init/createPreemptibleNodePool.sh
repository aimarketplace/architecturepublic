gcloud beta container \
--project "ai-marketplace-280309" node-pools create "preemptible-pool" \
--cluster "ai-marketplace" \
--zone "europe-west4-b" \
--node-version "1.16.9-gke.2" \
--machine-type "e2-micro" \
--image-type "COS" \
--disk-type "pd-standard" \
--disk-size "10" \
--node-labels type=preemptible \
--metadata disable-legacy-endpoints=true \
--scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
--preemptible \
--enable-autoscaling \
--num-nodes "0" \
--min-nodes "0" \
--max-nodes "4" \
--no-enable-autoupgrade \
--enable-autorepair \
--max-surge-upgrade 1 \
--max-unavailable-upgrade 0 \
--shielded-secure-boot

gcloud beta container \
--project "ai-marketplace-280309" node-pools create \
"gpu-pool" \
--cluster "ai-marketplace" \
--zone "europe-west4-b" \
--node-version "1.14.10-gke.36" \
--machine-type "n1-standard-1" \
--accelerator "type=nvidia-tesla-t4,count=1" \
--image-type "COS" \
--disk-type "pd-standard" \
--disk-size "50" \
--node-labels type=gpu \
--metadata disable-legacy-endpoints=true \
--scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
--preemptible \
--enable-autoscaling \
--num-nodes "0" \
--min-nodes "0" \
--max-nodes "1" \
--no-enable-autoupgrade \
--enable-autorepair \
--max-surge-upgrade 1 \
--max-unavailable-upgrade 0
minikube start
kubectl apply -f kubectl/namespace.yaml
kubectl apply -f kubectl/common.yaml
kubectl apply -f kubectl/initJWT.yaml
kubectl apply -f kubectl/dev.yaml
kubectl apply -f kubectl/ingress.yaml
kubectl expose deployment ingress-service --type=NodePort --port=8080
minikube service ingress-service --url

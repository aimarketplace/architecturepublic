minikube start
kubectl apply -f kubectl/namespaces.yaml
kubectl apply -f kubectl/common.yaml
kubectl apply -f kubectl/initJWT.yaml
kubectl apply -f kubectl/dev.yaml
kubectl apply -f kubectl/ingress.yaml
kubectl expose deployment ingress-service.dev --type=NodePort --port=8080
minikube service hello-minikube --url

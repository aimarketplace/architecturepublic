# README #

Settings for AI-Marketplace cluster setup

### How to start a new gcloud Kubernetes cluster ###

Start initialization:
1. Create Project in https://console.cloud.google.com/kubernetes/ (down triangle in 
top bar left next to current project name)
2. C�pen cloud shell (">" sign in top bar right)
3. Run createClusterAndPreemptiblePool.sh (in git directory cluster/init/)

 
kubectl apply -f https://bitbucket.org/aimarketplace/architecturepublic/raw/master/cluster/init/initializeCluster.yml
(To see logs:  kubectl logs initialize-cluster )



### Old description...

1. Cluster name: ai-marketplace
2. Use zonal cluster
3. Region europe-west3-a
4. NodePool: nodetype: e2-micro, premptible, 20GB boot disk (spinning)
5. connect to console:
   a) git clone https://aimarketplace@bitbucket.org/aimarketplace/architecturepublic.git 
   b) cd architecturepublic/
   c) kubectl apply -f kubectl.yaml
   d) gcloud compute addresses create ai-marketplace-ip --global
   e) Wait some minutes, then get IP Address from: kubectl get ingress ingress-service
   f) Strato->Domains Verwalten->A record: IP Adresse eintragen
   g) disable AUTH in ingress, upload http://ai-marketplace.de/resource/auth/authkeys.json, enable AUTH
   h) upload jars (trigger rebuild in all projects)
   